'use strict';

const webpack = require('webpack');
const merge = require('webpack-merge');
const baseWebpackConfig = require('./webpack.base.conf');
const devWebpackConfig = merge(baseWebpackConfig, {
    mode: 'development',

    devtool:'nosources-source-map',
    devServer: {
        port:8081,
        contentBase: baseWebpackConfig.externals.paths.dist,
        overlay: {
            warnings:false,
            errors:true,
        }   
    },
    watch:true,
    watchOptions:{
        ignored:[
            'node_modules',
            'server'
        ]
    }
});

module.exports = new Promise((resolve, reject) => {
    resolve(devWebpackConfig);
});