'use strict';

const path = require('path');
const fs = require('fs');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const PATHS = {
    src:path.join(__dirname, '../source'),
    dist:path.join(__dirname, '../server/public'),
    assets:'static'
}
const PAGES = `${PATHS.src}/pug/pages`;
const TEMPLATES = `${PATHS.src}/pug/templates`;

function generateHtmlPlugins() {

    const pageFiles = fs.readdirSync(PAGES);
    let plugins = pageFiles.map(item => {
        let parts = item.split('.');
        let name = parts[0];
        return new HtmlWebpackPlugin({
            filename: `./html/${name}.ejs`,
            template: path.resolve(__dirname, `${PAGES}/${name}.pug`),
            inject: false,
        })
    });

    const templateFiles = fs.readdirSync(TEMPLATES);
    plugins = plugins.concat(templateFiles.map(item => {
        let parts = item.split('.');
        let name = parts[0];
        return new HtmlWebpackPlugin({
            filename: `./html/${name}.ejs`,
            template: path.resolve(__dirname, `${TEMPLATES}/${name}.pug`),
            inject: false,
        })
    }));
    return plugins;

}

module.exports = {

    externals: {
        paths:PATHS
    },

    entry: {
        home: `${PATHS.src}/js/home.js`,
        news: [
            `@babel/polyfill`,
            `${PATHS.src}/js/news.js`
        ],
        admin_menu: [
            `@babel/polyfill`,
            `${PATHS.src}/js/admin_menu.js`
        ],
        admin_authorization: [
            `@babel/polyfill`,
            `${PATHS.src}/js/admin_authorization.js`
        ],
        admin_news: [
            `@babel/polyfill`,
            `${PATHS.src}/js/admin_news.js`
        ],
        admin_statistics: `${PATHS.src}/js/admin_statistics.js`
    },
    output: {
        filename:`${PATHS.assets}/js/[name].js`,
        path:PATHS.dist,
        publicPath:'/'
    },

    optimization: {
        splitChunks: {
            cacheGroups: {
                vendor: {
                    name: 'vendors',
                    test: /node_modules/,
                    chunks: 'all',
                    enforce: true
                }
            }
        }
    },

    module: {
        rules: [
            {
                test: /\.pug$/,
                loader:'pug-loader',
            },
            {
                test: /\.js$/,
                loader:'babel-loader',
                exclude: '/node_modules/',
            },
            {
                test: /\.(gif|png|jpe?g|svg)$/i,
                loader:'file-loader',
                options: {
                    name:'[name].[ext]'
                }
            },
            {
                test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)$/i,
                loader:'file-loader',
                options: {
                    name:'[name].[ext]'
                }
            },
            {
                test:/\.scss$/,
                use:[
                    'style-loader',
                    MiniCssExtractPlugin.loader,
                    {
                        loader:'css-loader',
                        options:{sourceMap:false}
                    },
                    {
                        loader:'postcss-loader',
                        options:{sourceMap:false, config: {path: './postcss.config.js'}}
                    },
                    {
                        loader:'sass-loader',
                        options:{sourceMap:false}
                    }
                ]
            }
        ]
    },

    plugins: [
        new MiniCssExtractPlugin({
           filename: `${PATHS.assets}/css/[name].css` 
        }),
        new CopyWebpackPlugin([
            {from: `${PATHS.src}/assets/img`, to:`${PATHS.assets}/img`},
            {from: `${PATHS.src}/assets/svg`, to:`${PATHS.assets}/img`},
            {from: `${PATHS.src}/static`, to:`${PATHS.assets}`},
            {from: `${PATHS.src}/assets/fonts`, to:`${PATHS.assets}/fonts`},
        ])
    ].concat(generateHtmlPlugins()),
}