import "../assets/scss/admin_authorization.scss"

addEventListener("DOMContentLoaded", () => {
    window.addEventListener("keypress", e => {
        if (event.charCode == 13) {
            verify();
        }
    });
    document.querySelector('.verify').addEventListener('click', () => verify());
});

async function verify() {
    let password = document.querySelector('.password').value;
    if (password.trim() != '') {
        let res = await (require('./modules/libs/xhr'))('GET', `/api/verifyAdmin/${password}`);
        console.log(res);
        if (res && res == 'true') document.location.href = '';
        else alert('Неверный пароль');
    }
    else {
        alert('Введите пароль');
    }
}