import '../assets/scss/home.scss';

class Page {
    constructor() {
        this.mobile = false;

        window.top.Modal = require('./modules/modals');
        this.getPositionTop = require('./modules/libs/getPositionTop');
        
        const animations = require('./modules/home/animations');
        const blocks = require('./modules/home/blocks');
        const menu = require('./modules/menu');

        // Aniamtions
        this.animations = new animations.Animations(this);
        this.parallax = new animations.Parallax(this);

        // Menu
        this.menu = new menu.Menu(this);
        this.adaptive = new menu.Adaptive(this);

        // Page
        this.scroll = new (require('./modules/home/scroll'))(this);
        this.slider = new (require('./modules/home/slider'))(this);
        this.volunteering = new blocks.Volunteering(this);
        this.lessons = new blocks.Lessons(this);
    }

}

addEventListener("DOMContentLoaded", () => {
    new Page();
});