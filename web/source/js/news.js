import '../assets/scss/news.scss';

class Page {
    constructor() {
        this.mobile = false;

        this.xhr = require('./modules/libs/xhr');
        this.getPositionTop = require('./modules/libs/getPositionTop');
        
        const menu = require('./modules/menu');

        // Menu
        this.menu = new menu.Menu(this);
        this.adaptive = new menu.Adaptive(this);

        // Page
        this.scroll = new (require('./modules/news/scroll'))(this);
        this.news = new (require('./modules/news/news'))(this);
    }
}

addEventListener("DOMContentLoaded", () => {
    new Page();
});