import "../assets/scss/admin_news.scss"

class Page {
    constructor() {
        window.top.Modal = require('./modules/modals');

        this.xhr = require('./modules/libs/xhr');

        this.addNews = new (require('./modules/admin_news/addNews'))(this);
        this.allNews = new (require('./modules/admin_news/allNews'))(this);
        this.editNews = new (require('./modules/admin_news/editNews'))(this);
    }
}

addEventListener("DOMContentLoaded", () => {
    new Page();
});