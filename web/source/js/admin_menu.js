import "../assets/scss/admin_menu.scss"

class Menu {
    constructor() {
        this.links = [
            {
                'title':'Статистика',
                'description':'Показать статистику посещения сайта',
                'href':'/admin/statistics'
            },
            {
                'title':'Волнотрество',
                'description':'Изменение блока "волентерство"',
                'href':'/admin/volunteering'
            },
            {
                'title':'Новости',
                'description':'Добавление и редактирование новостей',
                'href':'/admin/news'
            }
        ]
        this.menu = document.querySelector('.menu');
        this.addLinks();
    }
    addLinks() {
        this.links.forEach(value => {
            let a = document.createElement('a');
            let url = document.location.href.split('/');
            url = url.reduce((prev, value, index) => {
                if (index > 2) prev += `/${value}`;
                return prev;
            }, '');
            a.className = (value.href == url) ? 'menu-link this-menu-link' : 'menu-link';
            a.href = value.href;
            a.innerHTML = `<div class="menu-link-title">${value.title}</div>
            <div class="menu-link-description">${value.description}</div>`;
            this.menu.appendChild(a);
        });
    }
}

addEventListener("DOMContentLoaded", () => {
    new Menu();
});