class AllNews {
    constructor(page) {
        this.page = page;
        this.uploadNews();
    }
    async uploadNews() {
        document.querySelector('.all-news').innerHTML = '<h2 class="section-header">Загрузка...</h2>';
        this.news = JSON.parse(await this.page.xhr('GET', '/api/news/get?limit=30&columns=["title"]'));
        this.addOnPage();
    }
    addOnPage() {
        document.querySelector('.all-news').innerHTML = '<h2 class="section-header">Все новости</h2>';
        this.news.forEach(value => {
            let section = document.createElement('section');
            section.className = 'one-news';
            section.innerHTML = `<div class="one-news-title">${value.title}</div>`;
            
            let edit = document.createElement('button');
            edit.className = 'edit-one-news';
            edit.title = 'Редактировать';
            section.appendChild(edit);
            edit.onclick = () => this.page.editNews.open(value.id);

            let del = document.createElement('button');
            del.className = 'delete-one-news';
            del.title = 'Удалить';
            section.appendChild(del);
            del.onclick = () => this.deleteNews(value.id);

            document.querySelector('.all-news').appendChild(section);
        });
    }
    async deleteNews(id) {
        let res = JSON.parse(await this.page.xhr('GET', `/api/news/delete?find={"id":${id}}`));
        if (res && res[0] && res[0].id == id) {
            this.uploadNews();
        }
    }
}

module.exports = AllNews;