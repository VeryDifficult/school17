class EditNews {
    constructor(page) {
        this.page = page;
        this.file = [];
    }
    async open(id) {
        this.id = id;
        let news = JSON.parse(await this.page.xhr('GET', `/api/news/get?find={"id":${id}}`));
        if (news.length > 0) {
            let html = await this.page.xhr('GET', '/templates/editNews');
            Modal.open(html);
            document.querySelector('.edit-news-title').value = news[0].title;
            document.querySelector('.edit-news-text').value = news[0].text;
            this.preview = document.querySelector('.edit-news-picture-container');
            document.querySelector('#edit-news-picture').addEventListener('change', () => this.loadPreview());
            document.querySelector('.edit-news-button').addEventListener('click', () => this.save());
            let img = document.createElement('img');
            img.src = news[0].image;
            this.preview.innerHTML = '';
            this.preview.appendChild(img);
            img.onload = () => {
                img.style.left = `${(this.preview.offsetWidth - img.offsetWidth) / 2}px`;
            }
        }
    }
    async save() {
        let title = document.querySelector('.edit-news-title').value;
        let text = document.querySelector('.edit-news-text').value;
        if (!title || title.trim() == '') alert('Напишите название новости');
        else if (!text || text.trim() == '') alert('Введите основаной текст новости');
        else {
            let data = new FormData();
            data.append('title', title);
            data.append('text', text);
            data.append('id', this.id);
            if (this.file) data.append('file', this.file);
            this.loadTextPreview('Загрузка...');
            await this.page.xhr('POST', `/api/news/update`, data);
            this.page.allNews.uploadNews();
            this.file = false;
            Modal.close();
        }
    }
    loadPreview() {
        this.preview.innerHTML = 'Загрузка...';
        this.file = document.querySelector('#edit-news-picture').files[0];
        if (!this.file) this.loadTextPreview('Добавить изображение');
        else if (this.file.type != 'image/png' && this.file.type != 'image/jpeg') this.loadTextPreview('Выберите PNG или JPEG файл');
        else if (this.file.size  > 3 * 1024 * 1024) this.loadTextPreview('Изображение больше 3MB');
        else {
            let reader = new FileReader();
            reader.readAsDataURL(this.file);
            reader.onloadend = () => {
                let img = document.createElement('img');
                img.src = reader.result;
                this.preview.innerHTML = '';
                this.preview.appendChild(img);
                img.onload = () => {
                    img.style.left = `${(this.preview.offsetWidth - img.offsetWidth) / 2}px`;
                }
            }
        }
    }
    loadTextPreview(text) {
        this.preview.innerHTML = text;
        this.file = false;
    }
}

module.exports = EditNews;