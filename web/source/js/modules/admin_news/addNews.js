class AddNews {
    constructor(page) {
        this.page = page;
        this.file = false;
        this.preview = document.querySelector('.add-news-picture-container');
        document.querySelector('#add-news-picture').addEventListener('change', () => this.loadPreview());
        document.querySelector('.add-news-button').addEventListener('click', () => this.addNews());
    }
    loadPreview() {
        this.preview.innerHTML = 'Загрузка...';
        this.file = document.querySelector('#add-news-picture').files[0];
        if (!this.file) this.loadTextPreview('Добавить изображение');
        else if (this.file.type != 'image/png' && this.file.type != 'image/jpeg') this.loadTextPreview('Выберите PNG или JPEG файл');
        else if (this.file.size  > 3 * 1024 * 1024) this.loadTextPreview('Изображение больше 3MB');
        else {
            let reader = new FileReader();
            reader.readAsDataURL(this.file);
            reader.onloadend = () => {
                let img = document.createElement('img');
                img.src = reader.result;
                this.preview.innerHTML = '';
                this.preview.appendChild(img);
                img.onload = () => {
                    img.style.left = `${(this.preview.offsetWidth - img.offsetWidth) / 2}px`;
                }
            }
        }
    }
    loadTextPreview(text) {
        this.preview.innerHTML = text;
        this.file = false;
    }
    async addNews() {
        let title = document.querySelector('.add-news-title').value;
        let text = document.querySelector('.add-news-text').value;
        if (!title || title.trim() == '') alert('Напишите название новости');
        else if (!text || text.trim() == '') alert('Введите основаной текст новости');
        else if (!this.file) alert('Выберите изображение');
        else {
            let data = new FormData();
            data.append('title', title);
            data.append('text', text);
            data.append('file', this.file);
            document.querySelector('.add-news-title').value = '';
            document.querySelector('.add-news-text').value = '';
            this.loadTextPreview('Загрузка...');
            await this.page.xhr('POST', `/api/news/insert`, data);
            this.page.allNews.uploadNews();
            this.loadTextPreview('Добавить изображение');
            this.file = false;
        }
    }
}

module.exports = AddNews;