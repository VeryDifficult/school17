class Scroll {
    constructor(page) {
        this.page = page;
        document.querySelector('.logo').onclick = () => this.scrollTo(0);
    }
    scrollTo(y) {
        window.scrollTo({
            top:y,
            behavior:'smooth'
        });
    }
}

module.exports = Scroll;