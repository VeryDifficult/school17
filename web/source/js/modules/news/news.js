class News {
    constructor(page) {
        this.page = page;
        this.newsList = [];
        this.newsButtons = [];
        this.loadNews();    
        window.addEventListener('scroll', () => this.scroll());
    }
    async loadNews() {
        this.news = JSON.parse(await this.page.xhr('GET', `/api/news/get`));
        console.log(this.news);
        this.addNews();
        this.newsList.reverse();
        this.newsButtons.reverse();
    }
    addNews() {
        this.news.forEach(value => {
            let section = document.createElement('section');
            section.className = 'one-news';
            section.innerHTML = `<h3 class="one-news-title">${value.title}</h3>
            <div class="one-news-date">${value.date}</div>
            <img class="one-news-image" src="${value.image}">
            <div class="one-news-text">${value.text}</div>`;

            document.querySelector('.all-news').appendChild(section);
            this.newsList.push(section);
            
            let div = document.createElement('div');
            div.className = 'one-list-news';
            div.innerHTML = `<div class="one-list-news-title">${(value.title.length > 20) ? `${value.title.substr(0, 18)}...` : value.title}</div>`;
            
            let button = document.createElement('button');
            button.className = 'one-list-news-button';
            button.onclick = () => this.page.scroll.scrollTo(this.page.getPositionTop(section) - 100);
            div.appendChild(button);
            this.newsButtons.push(div);

            document.querySelector('.news-list').appendChild(div);
        });
    }
    scroll() {
        if (!this.page.mobile) {
            let scroll = window.scrollY + 200;
            let i = 0;
            for (; i < this.newsList.length; i++) {
                let elem = this.newsList[i];
                if (this.page.getPositionTop(elem) < scroll) {
                    this.newsButtons[i].className = 'one-list-news-selected';
                    break;
                }
            }
            if (this.thisNews != i) {
                this.thisNews = i;
                this.newsButtons.forEach((value, index) => {
                    if (index != i) value.className = 'one-list-news';
                });
            }
        }
    }
}

module.exports = News;