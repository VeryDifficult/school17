class Modal {
    static open(content) {
        let modal = document.querySelector('#modal');
        let modalContent = document.querySelector('#modal-content');
        if (!modal || !modalContent) {
            let res = this.create();
            modal = res.modal;
            modalContent = res.modalContent;
        }
        modalContent.innerHTML = (typeof content == 'string') ? content : '';
        if (typeof content == 'object') modalContent.appendChild(content);
        modal.className = 'opened-modal';
        document.querySelector('body').style.overflow = 'hidden';
    }
    static close() {
        document.querySelector('#modal').className = 'closed-modal';
        document.querySelector('body').style.overflow = 'auto';
    }
    static create() {
        let modal = document.querySelector('#modal');
        let modalContent = document.querySelector('#modal-content');
        let closeModal = document.querySelector('#close-modal');
        if (!modal) {
            modal = document.createElement('aside');
            modal.id = 'modal';
            document.querySelector('body').appendChild(modal);
            modal.addEventListener('click', e => {
                if (e.target == modal) {
                    Modal.close();
                }
            });
        }
        if (!modalContent) {
            modalContent = document.createElement('section');
            modalContent.id = 'modal-content';
            modal.appendChild(modalContent);
        }
        if (!closeModal) {
            closeModal = document.createElement('a');
            closeModal.id = 'close-modal';
            modal.appendChild(closeModal);
            closeModal.addEventListener('click', () => Modal.close());
        }
        
        return {modal:modal, modalContent:modalContent};
    }
}

module.exports = Modal;