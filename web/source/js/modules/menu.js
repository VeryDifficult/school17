class Adaptive {
    constructor(page) {
        this.page = page;
        this.pcWidth = 1224;

        this.checkResize();
        window.addEventListener('resize', () => this.checkResize());
    }
    checkResize() {
        this.page.mobile = (window.innerWidth < this.pcWidth) ? true : false;
        if (!this.page.mobile) this.page.menu.reset();
    }
}

class Menu {
    constructor(page) {
        this.page = page;
        this.links = {
            'Головна':'/',
            'Новини':'/news',
            'Посилання':'/',
            'Допомога':'/',
        }

        this.menu = document.querySelector('.top-menu');
        this.opened = false;
        this.openMenuButton = document.querySelector('#open-menu');

        this.addLinks();
        this.openMenuButton.onclick = () => this.openMenu();
    }
    addLinks() {
        for (let key in this.links) {
            let a = document.createElement('a');
            a.href = this.links[key];
            a.innerText = key;
            a.className = 'one-top-menu-link';
            this.menu.appendChild(a);
        }
    }
    openMenu() {
        if (this.page.mobile) {
            if (this.opened) {
                this.menu.style.left = '100%';
                this.menu.style.top = '50px';
                this.menu.style.height = 'calc(100% - 50px)';
                this.menu.style.borderBottomLeftRadius = '100%';
                this.openMenuButton.className = 'open-menu-button';
            }
            else {
                this.menu.style.left = '0px';
                this.menu.style.top = '50px';
                this.menu.style.height = 'calc(100% - 50px)';
                this.menu.style.borderBottomLeftRadius = '0px';
                this.openMenuButton.className = 'open-menu-button-s';
            }
            this.opened = !this.opened;
        }
        else {
            this.menu.style.top = '0px';
            this.menu.style.height = '60px';
            this.menu.style.borderBottomLeftRadius = '0%';
        }
    }
    reset() {
        this.menu.setAttribute('style', '');
        this.openMenuButton.className = 'open-menu-button';
    }
}

exports.Adaptive = Adaptive;
exports.Menu = Menu;