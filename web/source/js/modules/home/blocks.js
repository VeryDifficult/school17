class Volunteering {
    constructor(page) {
        this.page = page;
        this.volunteering = require('./volunteering');
        this.blocks = [];

        this.load();
        window.addEventListener('scroll', () => this.page.animations.volunteering(this.blocks));
    }
    load() {
        this.volunteering.forEach(value => {
            let section = document.createElement('section');
            section.className = 'one-volunteering';
            section.innerHTML = `<div class="one-volunteering-description">${value.header}</div>`;
            this.blocks.push(section);

            let div = document.createElement('div');
            div.className = 'one-volunteering-picture';
            div.innerHTML = `<img src="${value.img}">`;
            
            let button = document.createElement('button');
            button.className = 'open-volunteering';
            button.onclick = () => Modal.open(`<h3 class="modal-header">${value.header}</h3><div class="modal-picture"><img src="${value.img}"></div><div class="modal-text">${value.text}</div>`);
            div.appendChild(button);
            
            section.appendChild(div);
            document.querySelector('.volunteering').appendChild(section);
        });
    }
}

class Lessons {
    constructor(page) {
        this.page = page;
        this.lessons = [
            '/img/lessons/1.jpg',
            '/img/lessons/2.jpg',
            '/img/lessons/3.jpg',
            '/img/lessons/4.jpg',
            '/img/lessons/5.jpg',
            '/img/lessons/6.jpg',
            '/img/lessons/7.jpg',
            '/img/lessons/8.jpg',
            '/img/lessons/9.jpg'
        ];
        this.images = [];

        this.load();
        window.addEventListener('scroll', () => this.page.animations.lessonsPictures(this.images));
    }
    load() {
        this.lessons.forEach(value => {
            let div = document.createElement('div');
            div.className = 'one-lesson-picture';
            div.innerHTML = `<img src="${value}">`;
            this.images.push(div);
            document.querySelector('.lessons-pictures').appendChild(div);
        });
    }
}

exports.Volunteering = Volunteering;
exports.Lessons = Lessons; 