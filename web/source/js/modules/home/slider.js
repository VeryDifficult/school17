class Slider {
    constructor(page) {
        this.page = page;
        this.slides = [
            '/img/class/1.jpg',
            '/img/class/2.jpg',
            '/img/class/3.jpg',
            '/img/class/4.jpg',
            '/img/class/5.jpg',
            '/img/class/6.jpg',
            '/img/class/7.jpg',
            '/img/class/8.jpg'
        ];

        this.slider = document.querySelector('.teacher-slider');
        this.container = document.querySelector('.slides-container');
        this.canvas = document.querySelector('.slider-canvas');
        this.slidesCount = this.slides.length;
        this.opened = 0;
        this.slideWidth = 1600 / 3;
        this.slideHeight = 400;
        this.displayedSlides = 5;
        this.maxDistanceBetweenSlides = 150;
        this.minusBetweenSlides = 11;
        this.imageTransform = 0.15;
        this.blur = 1;
        this.PC = false;
        
        this.slidesPositions = [];
        for (let i = 0; i <= this.displayedSlides; i++) {
            this.slidesPositions.push({
                left:this.maxDistanceBetweenSlides + (this.maxDistanceBetweenSlides - this.minusBetweenSlides * i / 2) * (i - 1),
                width:this.slideWidth * (1 - i * this.imageTransform),
                height:this.slideHeight * (1 - i * this.imageTransform),
                blur:this.blur * (1 - (this.displayedSlides - i - 1) / (this.displayedSlides - 1)),
                topCoef:this.slideHeight * i * this.imageTransform,
                zIndex:this.slidesCount - i
            });
        }

        this.images = [];
        this.loadSlides();

        this.page.animations.displaySlider(this);
        window.addEventListener('scroll', () => this.page.parallax.sliderParallax(this));

        this.adaptation();
        window.addEventListener('resize', () => this.adaptation());

        this.canvas.addEventListener('touchstart', e => this.touchStart(e));
        this.canvas.addEventListener('touchend', () => this.touchEnd())
        this.canvas.addEventListener('touchmove', e => this.touchMove(e));
    }
    touchStart(event) {
        this.startTouch = event.touches[0].clientX;
        this.lastTouch = this.tStart;
    }
    touchMove(event) {
        let m = event.touches[0].clientX;
        let move = m - this.startTouch;
        this.lastTouch = m;
        this.container.style.transition = 'none';
        this.container.style.left = `calc(${-this.opened * 100}% + ${move}px)`;
    }
    touchEnd() {
        let difference = this.startTouch - this.lastTouch;
        if (difference >= 50) {
            this.nextSlide();
        }
        else if (difference <= -50) {
            this.prevSlide();
        }
        else this.page.animations.displaySlider(this);
    }
    adaptation() {
        this.screenCenter = window.innerHeight / 2 + 30;
        if (this.page.mobile && this.PC) {
            this.PC = false;
            this.slider.setAttribute('style', '');
            this.images.forEach(value => {
                value.setAttribute('style', '');
            });
            this.page.animations.displaySlider(this);
        }
        else if (!this.PC) {
            this.PC = true
            let last = this.slidesPositions[this.slidesPositions.length - 2];
            this.width = last.width + last.left;
            this.slider.style.width = `${this.width}px`;
            this.container.setAttribute('style', '');
            this.page.animations.displaySlider(this);
        }
    }
    nextSlide() {
        this.opened++;
        if (this.opened >= this.slidesCount) this.opened--;
        this.page.animations.displaySlider(this);
    }
    prevSlide() {
        this.opened--;
        if (this.opened < 0) this.opened++;
        this.page.animations.displaySlider(this);
    }
    loadSlides() {
        for (let index = 0; index < this.slidesCount; index++) {
            let div = document.createElement('div');
            div.className = 'one-teacher-slide';
            let img = document.createElement('img');
            img.src = this.slides[index];
            img.onclick = () => this.openSlide(index);
            div.appendChild(img);
            this.container.appendChild(div);
            this.images.push(div);
        }
    }
    openSlide(number) {
        this.opened = number;
        this.page.animations.displaySlider(this);
    }
}

module.exports = Slider;