class Animations {
    constructor(page) {
        this.page = page;
        window.addEventListener('scroll', () => {
            this.scroll = window.scrollY + window.innerHeight;
            this.mobile = page.mobile ? 60 : 0; 
            this.lessonsText();
            this.teacherText();
        });
    }
    lessonsText() {
        let div = document.querySelector('.lessons-info');
        let pos = this.page.getPositionTop(div);
        if (this.scroll - 150 > pos - this.mobile) {
            div.style.left = '0px';
            div.style.opacity = 1;
        }
        else if (this.scroll < pos) {
            div.style.left = '100px';
            div.style.opacity = 0;
        }
    }
    teacherText() {
        let div = document.querySelector('.teacher-info-text');
        let pos = this.page.getPositionTop(div);
        if (this.scroll - 100 > pos - this.mobile) {
            div.style.left = '0px';
            div.style.opacity = 1;
        }
        else if (this.scroll < pos) {
            div.style.left = '-100px';
            div.style.opacity = 0;
        }
    }
    lessonsPictures(elems) {
        let scrollTop = window.scrollY + window.innerHeight;
        elems.forEach(value => {
            let pos = this.page.getPositionTop(value);
            if (scrollTop - 150 > pos - this.mobile) {
                value.style.top = '0px';
                value.style.opacity = 1;
                value.style.transform = 'scale(1)';
            }
            else if (scrollTop < pos) {
                value.style.top = '30px';
                value.style.opacity = 0;
                value.style.transform = 'scale(0.8)';
            }
        });
    }
    volunteering(elems) {
        let scrollTop = window.scrollY + window.innerHeight;
        elems.forEach(value => {
            let pos = this.page.getPositionTop(value);
            if (scrollTop - 150 > pos - this.mobile) {
                value.style.top = '0px';
                value.style.opacity = 1;
                value.style.transform = 'scale(1)';
            }
            else if (scrollTop < pos) {
                value.style.top = '30px';
                value.style.opacity = 0;
                value.style.transform = 'scale(0.8)';
            }
        });
    }
    moveSliderImages(img, position, opacity, pos) {
        img.style.transition = '350ms';
        img.style.opacity = opacity;
        img.style.filter = `blur(${position.blur}px)`;
        img.style.left = `${position.left}px`;
        img.style.width = `${position.width}px`;
        img.style.height = `${position.height}px`;
        img.style.top = `${position.topCoef * pos}px`;
        img.style.zIndex = position.zIndex;
    }
    displaySlider(that) {
        if (!this.page.mobile) {
            let n, n2 = 0;
            if (that.opened + that.displayedSlides >= that.slidesCount) {
                n = that.slidesCount;
                n2 = that.opened + that.displayedSlides - that.slidesCount;
            }
            else {
                n = that.opened + that.displayedSlides;
            }
            that.imgs = [].concat(that.images.slice(that.opened, n)).concat(that.images.slice(0, n2));
            that.imgs2 = [].concat(that.images.slice(n, that.slidesCount)).concat(that.images.slice(n2, that.opened));

            let sliderCenter = this.page.getPositionTop(that.slider) + that.slider.offsetHeight / 2;
            let pos = 0.5 - (sliderCenter - that.screenCenter - window.scrollY) / (window.innerHeight - 60);
            if (pos < 0.05) pos = 0.05;
            else if (pos > 0.95) pos = 0.95;

            for (let i = 0; i < that.imgs.length; i++) {
                this.moveSliderImages(that.imgs[i], that.slidesPositions[i], 1, pos);
            }
            for (let i = 0; i < that.imgs2.length; i++) {
                let params = that.slidesPositions[that.displayedSlides];
                params.zIndex = -1 - i;
                this.moveSliderImages(that.imgs2[i], params, 0, pos);
            }
        }
        else {
            that.container.style.transition = '350ms';
            that.container.style.left = `-${that.opened  * 100}%`;
        }
    }
}

class Parallax {
    constructor(page) {
        this.page = page;
        this.parallaxSpeed = 6;
        window.addEventListener('scroll', () => this.aboutParallax(document.querySelector('.about')));
    }
    aboutParallax(el) {
        if (!this.page.mobile) {
            let top = window.scrollY;
            let y = el.getBoundingClientRect().y;
            let parallax = (top - y) / this.parallaxSpeed;
            el.style.backgroundPosition = `center calc(${parallax}px - (100vh - 60px) / ${this.parallaxSpeed})`;
        }
    }
    sliderParallax(that) {
        if (!this.page.mobile) {
            let sliderCenter = this.page.getPositionTop(that.slider) + that.slider.offsetHeight / 2;
            let pos = 0.5 - (sliderCenter - that.screenCenter - window.scrollY) / (window.innerHeight - 60);
            if (pos < 0.05) pos = 0.05;
            else if (pos > 0.95) pos = 0.95;

            for (let i = 0; i < that.imgs.length; i++) {
                that.imgs[i].style.transition = '0ms';
                that.imgs[i].style.top = `${that.slidesPositions[i].topCoef * pos}px`;
            }
            for (let i = 0; i < that.imgs2.length; i++) {
                that.imgs2[i].style.transition = '0ms';
                that.imgs2[i].style.top = `${that.slidesPositions[that.displayedSlides].topCoef * pos}px`;
            }
        }
    }
}

exports.Animations = Animations;
exports.Parallax = Parallax;