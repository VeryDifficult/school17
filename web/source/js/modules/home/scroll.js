class Scroll {
    constructor(page) {
        this.page = page;
        document.querySelector('.logo').onclick = () => this.scrollTo(0);
        document.querySelector('.more-button').onclick = () => this.scrollTo(this.page.mobile ? getPositionTop(document.querySelector('.about')) - 50 : window.innerHeight - 60);
    }
    scrollTo(y) {
        window.scrollTo({
            top:y,
            behavior:'smooth'
        });
    }
}

module.exports = Scroll;