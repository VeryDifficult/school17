async function xhr(method, url, body) {
    return new Promise(resolve => {
        let xhr = new XMLHttpRequest();
        xhr.open(method, url, true);
        xhr.onload = function() {
            resolve(this.responseText);
        }
        xhr.send(body || null);
    })
}

module.exports = xhr;