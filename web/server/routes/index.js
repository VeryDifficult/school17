const express = require('express');
const router = express.Router();

const main = require('../controllers/main');
const news = require('../controllers/news')
const admin = require('../controllers/admin');
const templates = require('../controllers/templates');

router.all('*', (req, res, next) => {
    if (req.headers.host.substr(0, 4) == 'api.') req.url = `/api${req.url}`;
    next();
});
router.all('*', admin.verify);
router.get('/homepage', (req, res, next) => {
    res.redirect('/');
    next();
});
router.get('/', (req, res, next) => {
    req.url = '/homepage';
    next();
});

router.get('/api/verifyAdmin/:password', admin.authorize);
router.get('/admin/:page', admin.open); 
router.get('/api/news/:func', news);
router.post('/api/news/:func', news);
router.get('/templates/:template', templates);

router.get(/\/admin|\/admin\//, admin.open);
router.get('/:page', main);


module.exports = router;