const Schema = require('../db/schema');

const schema = new Schema('news', [
    {
        title:'title',
        type:'string',
        required:true
    },
    {
        title:'text',
        type:'string',
        required:true
    },
    {
        title:'image',
        type:'string',
        required:false
    },
    {
        title:'date',
        type:'string',
        required:true
    }
]);

schema.set = function(params) {
    let d = new Date();
    let day = d.getDate();
    let month = d.getMonth() + 1;
    let hours = d.getHours();
    let minutes = d.getMinutes();
    day = (day < 10) ? `0${day}` : `${day}`;
    month = (month < 10) ? `0${month}` : `${month}`;
    hours = (hours < 10) ? `0${hours}` : `${hours}`;
    minutes = (minutes < 10) ? `0${minutes}` : `${minutes}`;
    let date = `${day}.${month}.${d.getFullYear()} ${hours}:${minutes}`;
    params.date = date;
    return params;
}

module.exports = {
    get:async function(req) {
        if (req.query.find) req.query.find = JSON.parse(req.query.find);
        if (req.query.columns) req.query.columns = JSON.parse(req.query.columns);
        return await schema.select(req.query);
    },
    insert:async function(req) {
        if (req.admin) {
            let file = req.files.file;
            if (file && file.mimetype == 'image/jpeg' || file && file.mimetype == 'image/png') {
                let id = await schema.insert({
                    title:req.body.title,
                    text:req.body.text
                });
                let url = `/news_images/${id.id}.jpg`;
                file.mv(`/app/server/public/static${url}`, err => {if (err) throw err});
                
                await schema.update({find:{id:id.id}, set:{image:url}});
                return 0;
            }
            else throw 400
        }
        else throw 403;
    },
    delete:async function(req) {
        if (req.admin) {
            if (req.query.find) req.query.find = JSON.parse(req.query.find);
            return await schema.remove(req.query)
        }
        else throw 403;
    },
    update:async function(req) {
        if (req.admin) {
            if (req.body.title && req.body.text && req.body.id) {
                let file = (req.files) ? req.files.file : false;
                if (file && file.mimetype == 'image/jpeg' || file && file.mimetype == 'image/png') {
                    file.mv(`/app/server/public/static/news_images/${req.body.id}.jpg`, err => {if (err) throw err});
                }
                await schema.update({
                    find:{
                        id:req.body.id
                    },
                    set:{
                        title:req.body.title,
                        text:req.body.text
                    }
                });
                return 0;
            }
            else throw 400;
        }
        else throw 403;
    }
}