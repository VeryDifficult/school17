const Schema = require('../db/schema');

const schema = new Schema('admin_authorization', [
    {
        title:'key',
        type:'string',
        required:true
    },
    {
        title:'ip',
        type:'string',
        required:true
    }
]);

module.exports = {
    verify:async function(req) {
        let ip = req.connection.remoteAddress;
        let token = req.cookies.admin_token;
        if (!token) throw 0;
        if (!ip) throw 0;
        let userToken = await schema.select({find:{ip:ip}});
        if (userToken.length > 0 && userToken[0].key == token) return 0;
        else throw 0;
    },
    check:async function(password) {
        if (password == 'lakupz9563') return true;
        else throw 0;
    },
    authorize:async function(req) {
        let ip = req.connection.remoteAddress;
        let token;
        let user = await schema.select({find:{ip:ip}});
        if (user.length > 0) {
            token = user[0].key;
        }
        else {
            token = `${Math.round(Math.random() * 100000000000)}`;
            await schema.insert({key:token, ip:ip});
        }
        return token;
    }
}