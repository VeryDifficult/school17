const Postgres = require('pg').Pool;

const config = require('../config/index');

const postgres = new Postgres({
    host:config.get('postgres:url'),
    port:config.get('postgres:port'),
    database:config.get('postgres:database'),
    user:config.get('postgres:user'),
    password:config.get('postgres:password')
});

module.exports = sql => {
    let promise = new Promise((res, err) => {
        postgres.query(sql, (error, response) => {
            if (error) err(error);
            else res(response.rows);
        });
    });
    return promise;
}