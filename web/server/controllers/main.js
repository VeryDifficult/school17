const fs = require('fs');

module.exports = (req, res, next) => {
    let url = req.params.page;
    fs.exists(`/app/server/public/html/${url}.ejs`, exists => {
        if (exists) res.render(url);
        else next();
    });
}