const fs = require('fs');

module.exports = function(req, res, next) {
    let url = req.params.template;
    fs.exists(`/app/server/public/html/${url}.ejs`, exists => {
        if (exists) res.render(url);
        else next();
    });
}