const news = require('../models/news');

module.exports = async function(req, res, next) {
    if (news[req.params.func]) {
        try {
            res.json(await news[req.params.func](req));
        }
        catch(err) {
            res.sendStatus(err);
        }
    }
    else res.sendStatus(400);
}