const admin = require('../models/admin');
const fs = require('fs');

module.exports = {
    open:async function(req, res, next) {
        if (req.admin) {
            if (!req.params.page) res.redirect('/admin/statistics');
            fs.exists(`/app/server/public/html/admin_${req.params.page}.ejs`, exists => {
                if (exists) res.render(`admin_${req.params.page}`);
                else next();
            });
        }
        else res.render('admin_authorization');
    },
    authorize:async function(req, res, next) {
        try {
            let response = await admin.check(req.params.password);
            if (response) {
                try {
                    let token = await admin.authorize(req);
                    res.cookie('admin_token', token, {maxAge: 99999999 + Date.now()});
                    res.send(true);
                }
                catch(err) {
                    res.sendStatus(500);
                }
            }
            else res.send(false);
        }
        catch(err) {
            res.send(false);
        }
    },
    verify:async function(req, res, next) {
        try {
            await admin.verify(req);
            req.admin = true;
        }
        catch(err) {
            req.admin = false;
        }
        next();
    }
}