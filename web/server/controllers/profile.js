const Schema = require('../db/schema');
const crypto = require('crypto');

function encryptPassword(password, salt) {
    return crypto.createHmac('sha1', salt).update(password).digest('hex');
}

const schema = new Schema('profile', [
    {
        title:'username',
        type:'string',
        required:true
    },
    {
        title:'password',
        type:'string',
        required:true
    },
    {
        title:'age',
        type:'number',
        required:true
    },
    {
        title:'salt',
        type:'string',
        required:true
    }
]);

schema.get = function(params) {
    params.map(value => {
        if (value.salt) value.salt = `It's a secret`;
        if (value.password) value.password = `It's a secret`;
        return value;
    })
    return params;
}

schema.set = function(params) {
    if (params.password) {
        params.salt = Math.round((Math.random() * 999999)).toString();
        params.password = encryptPassword(params.password, params.salt);
    }
    return params;
}

module.exports = (req, res, next) => {
    const functions = {
        get:async function() {
            try {
                res.json(await schema.select(req.query));
            }
            catch(err) {
                res.sendStatus(400);
            }
        },
        insert:async function() {
            try {
                let id = await schema.insert({
                    username:req.query.username,
                    password:req.query.password,
                    age:+req.query.age
                });
                res.json(id);
            }
            catch(err) {
                res.sendStatus(400);
            }
        },
        remove:async function() {
            try {
                let id = await schema.remove(req.query);
                res.json(id);
            }
            catch(err) {
                res.sendStatus(400);
            }
        },
        set:async function() {
            try {
                await schema.update(req.query);
                res.json(await schema.select());
            }
            catch(err) {
                res.sendStatus(400);
            }
        },
        verify:async function() {
            if (Math.random() > 0.5) res.send(true);
            else res.send(false);
        }
    }
    if (functions[req.params.func]) functions[req.params.func]().then(() => {}, err => {
        res.send(err);
    });
    else res.sendStatus(400);
}