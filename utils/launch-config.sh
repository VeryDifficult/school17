#!/bin/bash

echo "
{
    \"port\": 8081,
    \"postgres\": {
        \"url\": \"db\",
        \"port\": \"5432\",
        \"database\": \"postgres\",
        \"user\": \"postgres\",
        \"password\": \"$POSTGRES_PASSWORD\"
    }
}
" > /app/server/config/config.json