FROM node:current


WORKDIR /app
COPY package.json ./
COPY postcss.config.js ./
COPY .npmrc ./
COPY .babelrc ./
RUN npm install supervisor webpack webpack-cli -g
RUN npm install